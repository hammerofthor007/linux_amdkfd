/*
 * Copyright 2012 Advanced Micro Devices, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Authors: Alex Deucher
 */
#ifndef __CIK_REG_H__
#define __CIK_REG_H__

#define CIK_DIDT_IND_INDEX                        0xca00
#define CIK_DIDT_IND_DATA                         0xca04

#define CIK_DC_GPIO_HPD_MASK                      0x65b0
#define CIK_DC_GPIO_HPD_A                         0x65b4
#define CIK_DC_GPIO_HPD_EN                        0x65b8
#define CIK_DC_GPIO_HPD_Y                         0x65bc

#define CIK_GRPH_CONTROL                          0x6804
#       define CIK_GRPH_DEPTH(x)                  (((x) & 0x3) << 0)
#       define CIK_GRPH_DEPTH_8BPP                0
#       define CIK_GRPH_DEPTH_16BPP               1
#       define CIK_GRPH_DEPTH_32BPP               2
#       define CIK_GRPH_NUM_BANKS(x)              (((x) & 0x3) << 2)
#       define CIK_ADDR_SURF_2_BANK               0
#       define CIK_ADDR_SURF_4_BANK               1
#       define CIK_ADDR_SURF_8_BANK               2
#       define CIK_ADDR_SURF_16_BANK              3
#       define CIK_GRPH_Z(x)                      (((x) & 0x3) << 4)
#       define CIK_GRPH_BANK_WIDTH(x)             (((x) & 0x3) << 6)
#       define CIK_ADDR_SURF_BANK_WIDTH_1         0
#       define CIK_ADDR_SURF_BANK_WIDTH_2         1
#       define CIK_ADDR_SURF_BANK_WIDTH_4         2
#       define CIK_ADDR_SURF_BANK_WIDTH_8         3
#       define CIK_GRPH_FORMAT(x)                 (((x) & 0x7) << 8)
/* 8 BPP */
#       define CIK_GRPH_FORMAT_INDEXED            0
/* 16 BPP */
#       define CIK_GRPH_FORMAT_ARGB1555           0
#       define CIK_GRPH_FORMAT_ARGB565            1
#       define CIK_GRPH_FORMAT_ARGB4444           2
#       define CIK_GRPH_FORMAT_AI88               3
#       define CIK_GRPH_FORMAT_MONO16             4
#       define CIK_GRPH_FORMAT_BGRA5551           5
/* 32 BPP */
#       define CIK_GRPH_FORMAT_ARGB8888           0
#       define CIK_GRPH_FORMAT_ARGB2101010        1
#       define CIK_GRPH_FORMAT_32BPP_DIG          2
#       define CIK_GRPH_FORMAT_8B_ARGB2101010     3
#       define CIK_GRPH_FORMAT_BGRA1010102        4
#       define CIK_GRPH_FORMAT_8B_BGRA1010102     5
#       define CIK_GRPH_FORMAT_RGB111110          6
#       define CIK_GRPH_FORMAT_BGR101111          7
#       define CIK_GRPH_BANK_HEIGHT(x)            (((x) & 0x3) << 11)
#       define CIK_ADDR_SURF_BANK_HEIGHT_1        0
#       define CIK_ADDR_SURF_BANK_HEIGHT_2        1
#       define CIK_ADDR_SURF_BANK_HEIGHT_4        2
#       define CIK_ADDR_SURF_BANK_HEIGHT_8        3
#       define CIK_GRPH_TILE_SPLIT(x)             (((x) & 0x7) << 13)
#       define CIK_ADDR_SURF_TILE_SPLIT_64B       0
#       define CIK_ADDR_SURF_TILE_SPLIT_128B      1
#       define CIK_ADDR_SURF_TILE_SPLIT_256B      2
#       define CIK_ADDR_SURF_TILE_SPLIT_512B      3
#       define CIK_ADDR_SURF_TILE_SPLIT_1KB       4
#       define CIK_ADDR_SURF_TILE_SPLIT_2KB       5
#       define CIK_ADDR_SURF_TILE_SPLIT_4KB       6
#       define CIK_GRPH_MACRO_TILE_ASPECT(x)      (((x) & 0x3) << 18)
#       define CIK_ADDR_SURF_MACRO_TILE_ASPECT_1  0
#       define CIK_ADDR_SURF_MACRO_TILE_ASPECT_2  1
#       define CIK_ADDR_SURF_MACRO_TILE_ASPECT_4  2
#       define CIK_ADDR_SURF_MACRO_TILE_ASPECT_8  3
#       define CIK_GRPH_ARRAY_MODE(x)             (((x) & 0x7) << 20)
#       define CIK_GRPH_ARRAY_LINEAR_GENERAL      0
#       define CIK_GRPH_ARRAY_LINEAR_ALIGNED      1
#       define CIK_GRPH_ARRAY_1D_TILED_THIN1      2
#       define CIK_GRPH_ARRAY_2D_TILED_THIN1      4
#       define CIK_GRPH_PIPE_CONFIG(x)		 (((x) & 0x1f) << 24)
#       define CIK_ADDR_SURF_P2			 0
#       define CIK_ADDR_SURF_P4_8x16		 4
#       define CIK_ADDR_SURF_P4_16x16		 5
#       define CIK_ADDR_SURF_P4_16x32		 6
#       define CIK_ADDR_SURF_P4_32x32		 7
#       define CIK_ADDR_SURF_P8_16x16_8x16	 8
#       define CIK_ADDR_SURF_P8_16x32_8x16	 9
#       define CIK_ADDR_SURF_P8_32x32_8x16	 10
#       define CIK_ADDR_SURF_P8_16x32_16x16	 11
#       define CIK_ADDR_SURF_P8_32x32_16x16	 12
#       define CIK_ADDR_SURF_P8_32x32_16x32	 13
#       define CIK_ADDR_SURF_P8_32x64_32x32	 14
#       define CIK_GRPH_MICRO_TILE_MODE(x)       (((x) & 0x7) << 29)
#       define CIK_DISPLAY_MICRO_TILING          0
#       define CIK_THIN_MICRO_TILING             1
#       define CIK_DEPTH_MICRO_TILING            2
#       define CIK_ROTATED_MICRO_TILING          4

/* CUR blocks at 0x6998, 0x7598, 0x10198, 0x10d98, 0x11998, 0x12598 */
#define CIK_CUR_CONTROL                           0x6998
#       define CIK_CURSOR_EN                      (1 << 0)
#       define CIK_CURSOR_MODE(x)                 (((x) & 0x3) << 8)
#       define CIK_CURSOR_MONO                    0
#       define CIK_CURSOR_24_1                    1
#       define CIK_CURSOR_24_8_PRE_MULT           2
#       define CIK_CURSOR_24_8_UNPRE_MULT         3
#       define CIK_CURSOR_2X_MAGNIFY              (1 << 16)
#       define CIK_CURSOR_FORCE_MC_ON             (1 << 20)
#       define CIK_CURSOR_URGENT_CONTROL(x)       (((x) & 0x7) << 24)
#       define CIK_CURSOR_URGENT_ALWAYS           0
#       define CIK_CURSOR_URGENT_1_8              1
#       define CIK_CURSOR_URGENT_1_4              2
#       define CIK_CURSOR_URGENT_3_8              3
#       define CIK_CURSOR_URGENT_1_2              4
#define CIK_CUR_SURFACE_ADDRESS                   0x699c
#       define CIK_CUR_SURFACE_ADDRESS_MASK       0xfffff000
#define CIK_CUR_SIZE                              0x69a0
#define CIK_CUR_SURFACE_ADDRESS_HIGH              0x69a4
#define CIK_CUR_POSITION                          0x69a8
#define CIK_CUR_HOT_SPOT                          0x69ac
#define CIK_CUR_COLOR1                            0x69b0
#define CIK_CUR_COLOR2                            0x69b4
#define CIK_CUR_UPDATE                            0x69b8
#       define CIK_CURSOR_UPDATE_PENDING          (1 << 0)
#       define CIK_CURSOR_UPDATE_TAKEN            (1 << 1)
#       define CIK_CURSOR_UPDATE_LOCK             (1 << 16)
#       define CIK_CURSOR_DISABLE_MULTIPLE_UPDATE (1 << 24)

#define CIK_ALPHA_CONTROL                         0x6af0
#       define CIK_CURSOR_ALPHA_BLND_ENA          (1 << 1)

#define CIK_LB_DATA_FORMAT                        0x6b00
#       define CIK_INTERLEAVE_EN                  (1 << 3)

#define CIK_LB_DESKTOP_HEIGHT                     0x6b0c

#define KFD_CIK_SDMA_QUEUE_OFFSET		0x200

#define SQ_IND_INDEX					0x8DE0
#define SQ_CMD						0x8DEC
#define SQ_IND_DATA					0x8DE4

/*
 * The TCP_WATCHx_xxxx addresses that are shown here are in dwords,
 * and that's why they are multiplied by 4
 */
#define TCP_WATCH0_ADDR_H				(0x32A0*4)
#define TCP_WATCH1_ADDR_H				(0x32A3*4)
#define TCP_WATCH2_ADDR_H				(0x32A6*4)
#define TCP_WATCH3_ADDR_H				(0x32A9*4)
#define TCP_WATCH0_ADDR_L				(0x32A1*4)
#define TCP_WATCH1_ADDR_L				(0x32A4*4)
#define TCP_WATCH2_ADDR_L				(0x32A7*4)
#define TCP_WATCH3_ADDR_L				(0x32AA*4)
#define TCP_WATCH0_CNTL					(0x32A2*4)
#define TCP_WATCH1_CNTL					(0x32A5*4)
#define TCP_WATCH2_CNTL					(0x32A8*4)
#define TCP_WATCH3_CNTL					(0x32AB*4)

#define CPC_INT_CNTL					0xC2D0

#define CP_HQD_IQ_RPTR					0xC970u
#define AQL_ENABLE					(1U << 0)
#define SDMA0_RLC0_RB_CNTL				0xD400u
#define	RB_ENABLE					(1 << 0)
#define	RB_SIZE(x)					(x << 1)
#define	RB_SWAP_ENABLE					(1 << 9)
#define	RPTR_WRITEBACK_ENABLE				(1 << 12)
#define	RPTR_WRITEBACK_SWAP_ENABLE			(1 << 13)
#define	RPTR_WRITEBACK_TIMER(x)				(x << 16)
#define	RB_VMID(x)					(x << 24)
#define	SDMA0_RLC0_RB_BASE				0xD404u
#define	SDMA0_RLC0_RB_BASE_HI				0xD408u
#define	SDMA0_RLC0_RB_RPTR				0xD40Cu
#define	SDMA0_RLC0_RB_WPTR				0xD410u
#define	SDMA0_RLC0_RB_WPTR_POLL_CNTL			0xD414u
#define	SDMA0_RLC0_RB_WPTR_POLL_ADDR_HI			0xD418u
#define	SDMA0_RLC0_RB_WPTR_POLL_ADDR_LO			0xD41Cu
#define	SDMA0_RLC0_RB_RPTR_ADDR_HI			0xD420u
#define	SDMA0_RLC0_RB_RPTR_ADDR_LO			0xD424u
#define	SDMA0_RLC0_IB_CNTL				0xD428u
#define	SDMA0_RLC0_IB_RPTR				0xD42Cu
#define	SDMA0_RLC0_IB_OFFSET				0xD430u
#define	SDMA0_RLC0_IB_BASE_LO				0xD434u
#define	SDMA0_RLC0_IB_BASE_HI				0xD438u
#define	SDMA0_RLC0_IB_SIZE				0xD43Cu
#define	SDMA0_RLC0_SKIP_CNTL				0xD440u
#define	SDMA0_RLC0_CONTEXT_STATUS			0xD444u
#define	SELECTED					(1 << 0)
#define	IDLE						(1 << 2)
#define	EXPIRED						(1 << 3)
#define	EXCEPTION					(1 << 4)
#define	CTXSW_ABLE					(1 << 7)
#define	CTXSW_READY					(1 << 8)
#define	SDMA0_RLC0_DOORBELL				0xD448u
#define	OFFSET(x)					(x << 0)
#define	DB_ENABLE					(1 << 28)
#define	CAPTURED					(1 << 30)
#define	SDMA0_RLC0_VIRTUAL_ADDR				0xD49Cu
#define	ATC						(1 << 0)
#define	VA_PTR32					(1 << 4)
#define	VA_SHARED_BASE(x)				(x << 8)
#define	VM_HOLE						(1 << 30)
#define	SDMA0_RLC0_APE1_CNTL				0xD4A0u
#define	SDMA0_RLC0_DOORBELL_LOG				0xD4A4u
#define	SDMA0_RLC0_WATERMARK				0xD4A8u
#define	SDMA0_CNTL					0xD010
#define	SDMA1_CNTL					0xD810
#define	AUTO_CTXSW_ENABLE				(1 << 18)

enum {
	MAX_TRAPID = 8,		/* 3 bits in the bitfield.  */
	MAX_WATCH_ADDRESSES = 4
};

enum {
	ADDRESS_WATCH_REG_ADDR_HI = 0,
	ADDRESS_WATCH_REG_ADDR_LO,
	ADDRESS_WATCH_REG_CNTL,
	ADDRESS_WATCH_REG_MAX
};

enum {				/*  not defined in the CI/KV reg file  */
	ADDRESS_WATCH_REG_CNTL_ATC_BIT = 0x10000000UL,
	ADDRESS_WATCH_REG_CNTL_DEFAULT_MASK = 0x00FFFFFF,
	ADDRESS_WATCH_REG_ADDLOW_MASK_EXTENTION = 0x03000000,
	   /* extend the mask to 26 bits in order to match the low address field. */
	ADDRESS_WATCH_REG_ADDLOW_SHIFT = 6,
	ADDRESS_WATCH_REG_ADDHIGH_MASK = 0xFFFF
};

union TCP_WATCH_CNTL_BITS {
	struct {
		uint32_t mask:24;
		uint32_t vmid:4;
		uint32_t atc:1;
		uint32_t mode:2;
		uint32_t valid:1;
	} bitfields, bits;
	uint32_t u32All;
	signed int i32All;
	float f32All;
};

struct cik_mqd {
	uint32_t header;
	uint32_t compute_dispatch_initiator;
	uint32_t compute_dim_x;
	uint32_t compute_dim_y;
	uint32_t compute_dim_z;
	uint32_t compute_start_x;
	uint32_t compute_start_y;
	uint32_t compute_start_z;
	uint32_t compute_num_thread_x;
	uint32_t compute_num_thread_y;
	uint32_t compute_num_thread_z;
	uint32_t compute_pipelinestat_enable;
	uint32_t compute_perfcount_enable;
	uint32_t compute_pgm_lo;
	uint32_t compute_pgm_hi;
	uint32_t compute_tba_lo;
	uint32_t compute_tba_hi;
	uint32_t compute_tma_lo;
	uint32_t compute_tma_hi;
	uint32_t compute_pgm_rsrc1;
	uint32_t compute_pgm_rsrc2;
	uint32_t compute_vmid;
	uint32_t compute_resource_limits;
	uint32_t compute_static_thread_mgmt_se0;
	uint32_t compute_static_thread_mgmt_se1;
	uint32_t compute_tmpring_size;
	uint32_t compute_static_thread_mgmt_se2;
	uint32_t compute_static_thread_mgmt_se3;
	uint32_t compute_restart_x;
	uint32_t compute_restart_y;
	uint32_t compute_restart_z;
	uint32_t compute_thread_trace_enable;
	uint32_t compute_misc_reserved;
	uint32_t compute_user_data_0;
	uint32_t compute_user_data_1;
	uint32_t compute_user_data_2;
	uint32_t compute_user_data_3;
	uint32_t compute_user_data_4;
	uint32_t compute_user_data_5;
	uint32_t compute_user_data_6;
	uint32_t compute_user_data_7;
	uint32_t compute_user_data_8;
	uint32_t compute_user_data_9;
	uint32_t compute_user_data_10;
	uint32_t compute_user_data_11;
	uint32_t compute_user_data_12;
	uint32_t compute_user_data_13;
	uint32_t compute_user_data_14;
	uint32_t compute_user_data_15;
	uint32_t cp_compute_csinvoc_count_lo;
	uint32_t cp_compute_csinvoc_count_hi;
	uint32_t cp_mqd_base_addr_lo;
	uint32_t cp_mqd_base_addr_hi;
	uint32_t cp_hqd_active;
	uint32_t cp_hqd_vmid;
	uint32_t cp_hqd_persistent_state;
	uint32_t cp_hqd_pipe_priority;
	uint32_t cp_hqd_queue_priority;
	uint32_t cp_hqd_quantum;
	uint32_t cp_hqd_pq_base_lo;
	uint32_t cp_hqd_pq_base_hi;
	uint32_t cp_hqd_pq_rptr;
	uint32_t cp_hqd_pq_rptr_report_addr_lo;
	uint32_t cp_hqd_pq_rptr_report_addr_hi;
	uint32_t cp_hqd_pq_wptr_poll_addr_lo;
	uint32_t cp_hqd_pq_wptr_poll_addr_hi;
	uint32_t cp_hqd_pq_doorbell_control;
	uint32_t cp_hqd_pq_wptr;
	uint32_t cp_hqd_pq_control;
	uint32_t cp_hqd_ib_base_addr_lo;
	uint32_t cp_hqd_ib_base_addr_hi;
	uint32_t cp_hqd_ib_rptr;
	uint32_t cp_hqd_ib_control;
	uint32_t cp_hqd_iq_timer;
	uint32_t cp_hqd_iq_rptr;
	uint32_t cp_hqd_dequeue_request;
	uint32_t cp_hqd_dma_offload;
	uint32_t cp_hqd_sema_cmd;
	uint32_t cp_hqd_msg_type;
	uint32_t cp_hqd_atomic0_preop_lo;
	uint32_t cp_hqd_atomic0_preop_hi;
	uint32_t cp_hqd_atomic1_preop_lo;
	uint32_t cp_hqd_atomic1_preop_hi;
	uint32_t cp_hqd_hq_status0;
	uint32_t cp_hqd_hq_control0;
	uint32_t cp_mqd_control;
	uint32_t cp_mqd_query_time_lo;
	uint32_t cp_mqd_query_time_hi;
	uint32_t cp_mqd_connect_start_time_lo;
	uint32_t cp_mqd_connect_start_time_hi;
	uint32_t cp_mqd_connect_end_time_lo;
	uint32_t cp_mqd_connect_end_time_hi;
	uint32_t cp_mqd_connect_end_wf_count;
	uint32_t cp_mqd_connect_end_pq_rptr;
	uint32_t cp_mqd_connect_end_pq_wptr;
	uint32_t cp_mqd_connect_end_ib_rptr;
	uint32_t reserved_96;
	uint32_t reserved_97;
	uint32_t reserved_98;
	uint32_t reserved_99;
	uint32_t iqtimer_pkt_header;
	uint32_t iqtimer_pkt_dw0;
	uint32_t iqtimer_pkt_dw1;
	uint32_t iqtimer_pkt_dw2;
	uint32_t iqtimer_pkt_dw3;
	uint32_t iqtimer_pkt_dw4;
	uint32_t iqtimer_pkt_dw5;
	uint32_t iqtimer_pkt_dw6;
	uint32_t reserved_108;
	uint32_t reserved_109;
	uint32_t reserved_110;
	uint32_t reserved_111;
	uint32_t queue_doorbell_id0;
	uint32_t queue_doorbell_id1;
	uint32_t queue_doorbell_id2;
	uint32_t queue_doorbell_id3;
	uint32_t queue_doorbell_id4;
	uint32_t queue_doorbell_id5;
	uint32_t queue_doorbell_id6;
	uint32_t queue_doorbell_id7;
	uint32_t queue_doorbell_id8;
	uint32_t queue_doorbell_id9;
	uint32_t queue_doorbell_id10;
	uint32_t queue_doorbell_id11;
	uint32_t queue_doorbell_id12;
	uint32_t queue_doorbell_id13;
	uint32_t queue_doorbell_id14;
	uint32_t queue_doorbell_id15;
};

struct cik_sdma_rlc_registers {
	uint32_t sdma_rlc_rb_cntl;
	uint32_t sdma_rlc_rb_base;
	uint32_t sdma_rlc_rb_base_hi;
	uint32_t sdma_rlc_rb_rptr;
	uint32_t sdma_rlc_rb_wptr;
	uint32_t sdma_rlc_rb_wptr_poll_cntl;
	uint32_t sdma_rlc_rb_wptr_poll_addr_hi;
	uint32_t sdma_rlc_rb_wptr_poll_addr_lo;
	uint32_t sdma_rlc_rb_rptr_addr_hi;
	uint32_t sdma_rlc_rb_rptr_addr_lo;
	uint32_t sdma_rlc_ib_cntl;
	uint32_t sdma_rlc_ib_rptr;
	uint32_t sdma_rlc_ib_offset;
	uint32_t sdma_rlc_ib_base_lo;
	uint32_t sdma_rlc_ib_base_hi;
	uint32_t sdma_rlc_ib_size;
	uint32_t sdma_rlc_skip_cntl;
	uint32_t sdma_rlc_context_status;
	uint32_t sdma_rlc_doorbell;
	uint32_t sdma_rlc_virtual_addr;
	uint32_t sdma_rlc_ape1_cntl;
	uint32_t sdma_rlc_doorbell_log;
	uint32_t reserved_22;
	uint32_t reserved_23;
	uint32_t reserved_24;
	uint32_t reserved_25;
	uint32_t reserved_26;
	uint32_t reserved_27;
	uint32_t reserved_28;
	uint32_t reserved_29;
	uint32_t reserved_30;
	uint32_t reserved_31;
	uint32_t reserved_32;
	uint32_t reserved_33;
	uint32_t reserved_34;
	uint32_t reserved_35;
	uint32_t reserved_36;
	uint32_t reserved_37;
	uint32_t reserved_38;
	uint32_t reserved_39;
	uint32_t reserved_40;
	uint32_t reserved_41;
	uint32_t reserved_42;
	uint32_t reserved_43;
	uint32_t reserved_44;
	uint32_t reserved_45;
	uint32_t reserved_46;
	uint32_t reserved_47;
	uint32_t reserved_48;
	uint32_t reserved_49;
	uint32_t reserved_50;
	uint32_t reserved_51;
	uint32_t reserved_52;
	uint32_t reserved_53;
	uint32_t reserved_54;
	uint32_t reserved_55;
	uint32_t reserved_56;
	uint32_t reserved_57;
	uint32_t reserved_58;
	uint32_t reserved_59;
	uint32_t reserved_60;
	uint32_t reserved_61;
	uint32_t reserved_62;
	uint32_t reserved_63;
	uint32_t reserved_64;
	uint32_t reserved_65;
	uint32_t reserved_66;
	uint32_t reserved_67;
	uint32_t reserved_68;
	uint32_t reserved_69;
	uint32_t reserved_70;
	uint32_t reserved_71;
	uint32_t reserved_72;
	uint32_t reserved_73;
	uint32_t reserved_74;
	uint32_t reserved_75;
	uint32_t reserved_76;
	uint32_t reserved_77;
	uint32_t reserved_78;
	uint32_t reserved_79;
	uint32_t reserved_80;
	uint32_t reserved_81;
	uint32_t reserved_82;
	uint32_t reserved_83;
	uint32_t reserved_84;
	uint32_t reserved_85;
	uint32_t reserved_86;
	uint32_t reserved_87;
	uint32_t reserved_88;
	uint32_t reserved_89;
	uint32_t reserved_90;
	uint32_t reserved_91;
	uint32_t reserved_92;
	uint32_t reserved_93;
	uint32_t reserved_94;
	uint32_t reserved_95;
	uint32_t reserved_96;
	uint32_t reserved_97;
	uint32_t reserved_98;
	uint32_t reserved_99;
	uint32_t reserved_100;
	uint32_t reserved_101;
	uint32_t reserved_102;
	uint32_t reserved_103;
	uint32_t reserved_104;
	uint32_t reserved_105;
	uint32_t reserved_106;
	uint32_t reserved_107;
	uint32_t reserved_108;
	uint32_t reserved_109;
	uint32_t reserved_110;
	uint32_t reserved_111;
	uint32_t reserved_112;
	uint32_t reserved_113;
	uint32_t reserved_114;
	uint32_t reserved_115;
	uint32_t reserved_116;
	uint32_t reserved_117;
	uint32_t reserved_118;
	uint32_t reserved_119;
	uint32_t reserved_120;
	uint32_t reserved_121;
	uint32_t reserved_122;
	uint32_t reserved_123;
	uint32_t reserved_124;
	uint32_t reserved_125;
	uint32_t reserved_126;
	uint32_t reserved_127;
	uint32_t sdma_engine_id;
	uint32_t sdma_queue_id;
};

#endif
